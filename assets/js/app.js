/*

  _Project! 
     __   _              ___ 
    / /  (_)__ ___ _    / _ \
   / /__/ / _ `/  ' \  / , _/
  /____/_/\_,_/_/_/_/ /_/|_|
  
*/

// --------------------------------------------------------------------------------------------------------------

//CONFIGURE REQUIRE js

require.config({
    paths : {
        /*jQuery*/
        'jquery': 'vendor/jquery',

        /*Libs*/
        'imagesloaded': 'libs/jquery.imagesloaded.min',
        'breakpoints': 'libs/breakpoints',
        'waypoints': 'libs/jquery.waypoints.min',
        'royalslider': 'libs/jquery.royalslider.min',
        'nprogress': 'libs/nprogress.min',
        'enquire': 'libs/enquire.min',
        'svgeezy': 'libs/svgeezy.min',

        /* Foundation */
        'foundation': 'vendor/foundation.min',
        'foundation.core': 'vendor/foundation/foundation',
        'foundation.abide': 'vendor/foundation/foundation.abide',
        'foundation.accordion': 'vendor/foundation/foundation.accordion',
        'foundation.alert': 'vendor/foundation/foundation.alert',
        'foundation.clearing': 'vendor/foundation/foundation.clearing',
        'foundation.dropdown': 'vendor/foundation/foundation.dropdown',
        'foundation.equalizer': 'vendor/foundation/foundation.equalizer',
        'foundation.interchange': 'vendor/foundation/foundation.interchange',
        'foundation.joyride': 'vendor/foundation/foundation.joyride',
        'foundation.magellan': 'vendor/foundation/foundation.magellan',
        'foundation.offcanvas': 'vendor/foundation/foundation.offcanvas',
        'foundation.orbit': 'vendor/foundation/foundation.orbit',
        'foundation.reveal': 'vendor/foundation/foundation.reveal',
        'foundation.tab': 'vendor/foundation/foundation.tab',
        'foundation.tooltip': 'vendor/foundation/foundation.tooltip',
        'foundation.topbar': 'vendor/foundation/foundation.topbar',

        /* Vendor Scripts */
        'jquery.cookie': 'vendor/jquery.cookie',
        'fastclick': 'vendor/fastclick',
        'modernizr': 'vendor/modernizr',
        'placeholder': 'vendor/placeholder'
        
    },
    'shim': {
        'imagesLoaded': ['jquery'],

        /* Foundation */
        'foundation.core': {
            deps: [
            'jquery',
            'modernizr'
            ],
            exports: 'Foundation'
        },
        'foundation.abide': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.accordion': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.alert': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.clearing': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.dropdown': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.equalizer': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.interchange': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.joyride': {
            deps: [
            'foundation.core',
            'foundation.cookie'
            ]
        },
        'foundation.magellan': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.offcanvas': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.orbit': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.reveal': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.tab': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.tooltip': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.topbar': {
            deps: [
            'foundation.core'
            ]
        },

        /* Vendor Scripts */
        'jquery.cookie': {
            deps: [
            'jquery'
            ]
        },
        'fastclick': {
            exports: 'FastClick'
        },
        'modernizr': {
            exports: 'Modernizr'
        },
        'placeholder': {
            exports: 'Placeholders'
        }
    },
    priority : [
    'jquery'  //execute jquery before any other dependency
    ]
});

//APP

APP = {
  initial_run : true,
  //root : window.data.template_url,
  isotope_is_setup: false,

  all_blocks: null,

  controlled_scrolling: false,

  progress: {
    start: function(){

    },
    done: function(){

    }
  },

  common: {
    init: function() {

      log("APP.js Loaded");

      $.extend(jQuery.easing,
      {
        def: 'easeInOutQuint',

        easeInOutQuint: function (x, t, b, c, d) {

          if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;

          return c/2*((t-=2)*t*t*t*t + 2) + b;

        }
        
      });

      function debouncer( func , timeout ) {
         var timeoutID , timeout = timeout || 0;
         return function () {
            var scope = this , args = arguments;
            clearTimeout( timeoutID );
            timeoutID = setTimeout( function () {
                func.apply( scope , Array.prototype.slice.call( args ) );
            } , timeout );
         }
      }

      //VARS

      var win = $(window);

      APP.window = $(window);
      APP.document = $(document);
      APP.html = $('html');
      APP.body = $('body');
      APP.grid = $('.grid');
      APP.Modernizr = window.Modernizr;
      APP.history = window.history;

      APP.init_loader();

      //BREAKPOINTS

      /*$(window).resetBreakpoints();
      $(window).setBreakpoints({
      // use only largest available vs use all available
          distinct: true, 
      // array of widths in pixels where breakpoints
      // should be triggered
          breakpoints: [
              640,
              960,
              1440
          ] 
      });
      //Small
      $(window).bind('enterBreakpoint640',function() {
      });
      $(window).bind('exitBreakpoint640',function() {
      });*/

      //ENQUIRE/
      var small = "screen and (max-width:40em)";
      var medium = "screen and (min-width: 40em) and (max-width:64em)";
      var large = "screen and (min-width: 64em) and (max-width:90em)";
      var xlarge = "screen and (min-width: 90em) and (max-width:120em)";
      var xxlarge = "screen and (min-width: 120em)";

      var desktop = "screen and (min-width: 64em)";

      enquire.register(desktop, {

          // OPTIONAL
          // If supplied, triggered when a media query matches.
          match : function() {
            log(' ** BP: Desktop ** ');

            if($('html').hasClass('ie8') || $('html').hasClass('ie7')){
              $('body').removeClass('desktop');
            } else {

              $('body').addClass('desktop');
            }


          },      
                                      
          // OPTIONAL
          // If supplied, triggered when the media query transitions 
          // *from a matched state to an unmatched state*.
          unmatch : function() {
            log(' ** Unmatech: BP: Desktop ** ');

            $('body').removeClass('desktop');
            //$(window).off('resize.desktop');
            


          },    
          
          // OPTIONAL
          // If supplied, triggered once, when the handler is registered.
          setup : function() {
          },    
                                      
          // OPTIONAL, defaults to false
          // If set to true, defers execution of the setup function 
          // until the first time the media query is matched
          deferSetup : true,
                                      
          // OPTIONAL
          // If supplied, triggered when handler is unregistered. 
          // Place cleanup code here
          destroy : function() {}
            
      });

      

      enquire.register(small, function() { log(" * BP: Small * "); });
      enquire.register(medium, function() { log(" * BP: Medium * "); });
      enquire.register(large, function() { log(" * BP: Large * "); });
      enquire.register(xlarge, function() { log(" * BP: XLarge * "); });
      enquire.register(xxlarge, function() { log(" * BP: XXLarge * "); });

      var offset = 0;

      $(window).on('resize.desktop', debouncer(function(){
        if($(window).height() <= 900){
          $('body').removeClass('desktop');
        } else if($(window).height() > 900 && $(window).width() >= 1024) {
          $('body').addClass('desktop');
        }

        if($('html').hasClass('ie8') || $('html').hasClass('ie7')){
          $('body').removeClass('desktop');
        }

        if($('body').hasClass('desktop')){
          var page_height = $(window).height() - $('nav.main').height();
          var page_offset = $('nav.main').height();
          if(page_height < 700){
            page_height = 700;
          }
          $('.page').addClass('fixed').height(page_height);
          //$('.wrapper').css('top', page_offset);

          //$('.intro').height($(window).height());

          $('.page').width($(window).width());
          //$('.page-slider').width($(window).width() * $('.page-slider > li').length);

          offset = page_height;

          $('.home').waypoint(function(direction) {
            if(direction == "down"){
              $('body').attr('bg-colour', 'blue');
            }
          }, { offset: -20 });

          $('.projects').waypoint(function(direction) {
            if(direction == "down"){
              $('body').attr('bg-colour', 'white');
            }
          }, { offset: 100 });

          $('.people').waypoint(function(direction) {
            if(direction == "down"){
              $('body').attr('bg-colour', 'green');
            }
          }, { offset: 100 });

          $('.contact').waypoint(function(direction) {
            if(direction == "down"){
              $('body').attr('bg-colour', 'contact');
            }
          }, { offset: 100 });



          $('.home').waypoint(function(direction) {
            if(direction == "up"){
              $('body').attr('bg-colour', 'blue');
            }
          }, { offset: '-25%' });

          $('.projects').waypoint(function(direction) {
            if(direction == "up"){
              $('body').attr('bg-colour', 'white');
            }
          }, { offset: '-25%' });

          $('.people').waypoint(function(direction) {
            if(direction == "up"){
              $('body').attr('bg-colour', 'green');
            }
          }, { offset: '-25%' });

          $('.contact').waypoint(function(direction) {
            if(direction == "up"){
              $('body').attr('bg-colour', 'contact');
            }
          }, { offset: -100 });



          $('.project-gallery-list').addClass('rsDefault').royalSlider({
            autoHeight: false,
            arrowsNav: true,
            arrowsNavAutoHide: false,
            autoScaleSlider: false,
            fadeinLoadedSlide: false,
            imageScaleMode: 'fill',
            imageAlignCenter: true,
            loop: true,
            loopRewind: false,
            numImagesToPreload: 6,
            keyboardNavEnabled: true,
            slidesSpacing: 0,
            sliderDrag:false,

          });

        } else {

            setTimeout(function(){
              $('.page').removeClass('fixed').attr('style', '');
              $('.project-gallery-list').removeClass('rsDefault').each(function(){
                var rs = $(this).data('royalSlider');
                rs.destroy(false);
              });
              $('.project-slide').unwrap().unwrap().unwrap();
              $('.project-gallery-list .rsArrow').remove();
            },200);

        }


      }));

      $(window).trigger('resize.desktop');

      //U*RL

      $(window).hashchange(function(e) {
        get_url();
      });



      var last_scroll = 0;

      $(window).scroll(function() {

        

        var d_scroll = $(window).scrollTop();
        d_scroll = Math.max(d_scroll, 0);
        var scrolldelta = d_scroll - last_scroll;

        if(APP.controlled_scrolling == false){

          if(Math.abs(scrolldelta)>100){
            history.pushState("", document.title, window.location.pathname);
          }

        }

        
        last_scroll = d_scroll;

        

      });

      function get_url(){


        log('get url');

        if(window.location.hash) {
            var hash = window.location.hash.substring(1);
            var url_array = hash.split('/');

            var main_address = '#' + url_array[0];

            //update nav
            $('nav.main ul li').each(function(){
              
              if($(this).find('a').attr('href') == main_address){
                $(this).addClass('active')
              } else{
                $(this).removeClass('active');
              }
            });

            if(url_array.length > 0){

              var main_page = $('#' + 'page-'+url_array[0]);
              
              log(url_array[0]);

              //Main Area
              APP.controlled_scrolling = true;
              if($('nav.main').css('position') == "fixed"){
                if(url_array[0] == "contact"){
                  $("html, body").animate({ scrollTop: $(document).height() }, 1000, function(){
                    setTimeout(function(){
                      APP.controlled_scrolling = false;
                    }, 250);
                  });
                } else {
                  $("html, body").animate({ scrollTop: main_page.offset().top - ($('nav.main').height() - 10) }, 1000, function(){
                    setTimeout(function(){
                      APP.controlled_scrolling = false;
                    }, 250);
                  });
                }
              } else {
                $("html, body").animate({ scrollTop: main_page.offset().top + 30 }, 1000, function(){
                  setTimeout(function(){
                      APP.controlled_scrolling = false;
                    }, 250);
                });
              }
              if(url_array.length > 1){
                var sec_page = '#'+ 'subpage-' + url_array[1];
                $(window).trigger('resize.desktop');
                    //var pageslider = main_page.find('.page-slider');
                //var index = pageslider.find(sec_page).index();
                //pageslider.stop().animate({'margin-left':-(index * 100) + '%'}, 1000, "easeInOutQuint");
                main_page.find('.sub-page:not(:first)').animate({'opacity' : 0}, 400, function(){
                  main_page.find('.sub-page:not(:first)').css('display', 'none');
                });
                main_page.find('.sub-page:first').animate({'opacity' : 0}, 400, function(){
                  main_page.find('.sub-page:first').css('display', 'none');
                  $(sec_page).css({'display': 'block', 'opacity' : 0}).animate({'opacity' : 1}, 400, function(){ $(window).trigger('resize'); });
                  $(window).trigger('resize');
                  
                });
              } else {
                log('t');
                main_page.find('.sub-page:not(:first)').animate({'opacity' : 0}, 400, function(){
                  main_page.find('.sub-page:not(:first)').css('display', 'none');
                  main_page.find('.sub-page:first').css('display', 'block').animate({'opacity' : 1}, 400);
                });
              }

            } else {
              $("html, body").stop().animate({ scrollTop: 0  }, 1000);
            }

            // hash found
        } else {
            // No hash found
        }
        
      }

      setTimeout(function(){
          get_url();
      }, 500);

      



      //BEHAVIOURS
      APP.LoadBehavior();

      //Run AJAXIFY once
      /*if(APP.initial_run){
        require(['ajaxify'], function(module){

          //module.init();

        });

      }*/

      //RESIZE

      APP.window.on('resize', function(){

      });

      $(window).trigger('resize');

      APP.initial_run = false;

      

    },
  },

  init_loader: function(){

      //LOADER
      var loader = $('#preloader');
      loader.addClass('done');
      
      loader.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function () {
        loader.remove();
      });

      //ie
      setTimeout(function(){
        loader.remove();
      }, 800);

    }

};

//BEHAVIOURS

/* look through the document (or ajax'd in content if "context" is defined) to look for "data-behavior" attributes.
Initialize a new instance of the method if found, passing through the element that had the attribute
So in this example it will find 'data-behavior="show_articles"' and run the show_articles method. 
*/
APP.LoadBehavior = function(context){
  if(context === undefined){
    context = $(document);
  }

  context.find("*[data-behavior]").each(function(){
    var that = $(this);
    var behaviors = that.attr('data-behavior');

    $.each(behaviors.split(" "), function(index,behaviorName){
       
      require(['behaviors/' + behaviorName], function(module){

          //Init Module

          try {
            module.init(that);
          }
          catch(e){
            // No Operation
            log('error init: ' + e);
          }

          

      });


    });
  });
};

AJAX = {
  // Ajaxify Helper
  ajaxify: function(_this){

      // Prepare
      var $this = _this;

      // Ajaxify
      $this.find('a:not(.no-ajaxy)').click(function(event){
          // Prepare
          var
              $this = $(this),
              url = $this.attr('href'),
              title = $this.attr('title')||null;
          
          // Continue as normal for cmd clicks etc
          if ( event.which == 2 || event.metaKey ) { return true; }
          
          // Ajaxify this link
          History.pushState(null,title,url);
          event.preventDefault();
          return false;
      });
      
      // Chain
      return $this;
  }
}

UTIL = {
  exec: function( controller, action ) {
    var ns = APP,
        action = ( action === undefined ) ? "init" : action;

    if ( controller !== "" && ns[controller] && typeof ns[controller][action] == "function" ) {
      ns[controller][action]();
    }
  },

  init: function() {
    var body = document.body,
        controller = body.getAttribute( "data-controller" ),
        action = body.getAttribute( "data-action" );

    UTIL.exec( "common" );
    UTIL.exec( controller );
    UTIL.exec( controller, action );
  }
};

//INIT

require(['jquery', 'modernizr', 'svgeezy', 'fastclick'], function($){

    svgeezy.init(false, 'png');

    (function($) {

      $.fn.visible = function(partial) {
        
          var $t            = $(this),
              $w            = $(window),
              viewTop       = $w.scrollTop(),
              viewBottom    = viewTop + $w.height(),
              _top          = $t.offset().top,
              _bottom       = _top + $t.height(),
              compareTop    = partial === true ? _bottom : _top,
              compareBottom = partial === true ? _top : _bottom;
        
        return ((compareBottom <= (viewBottom)) && (compareTop >= viewTop));

      };
        
    })(jQuery);

    (function($,sr){

      // debouncing function from John Hann
      // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
      var debounce = function (func, threshold, execAsap) {
          var timeout;

          return function debounced () {
              var obj = this, args = arguments;
              function delayed () {
                  if (!execAsap)
                      func.apply(obj, args);
                  timeout = null;
              };

              if (timeout)
                  clearTimeout(timeout);
              else if (execAsap)
                  func.apply(obj, args);

              timeout = setTimeout(delayed, threshold || 100);
          };
      }
      // smartresize 
      jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

    })(jQuery,'smartresize');

      Modernizr.load([
        //first test need for polyfill
        {
            test: window.matchMedia,
            nope: ["assets/js/libs/matchMedia.js", "assets/js/libs/matchMedia.addListener.js"],
            complete : function () {

              require(['jquery', 'imagesloaded', 'breakpoints', 'foundation.reveal', 'waypoints', 'libs/jquery.hash.min', 'royalslider', 'enquire'], function($, imagesLoaded, breakpoints, r) {
          
                $(document).ready(function(){        

                  //Foundation 
                  $(document).foundation({});
                  
                  UTIL.init();
                });
              });

            }
        }

        
    ]);

    

});



// UTILS

APP.ua = navigator.userAgent;
APP.click_event = (is_touch_device()) ? "touchstart" : "click";

// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};

function isIpad() {
  return !!navigator.userAgent.match(/iPad/i);
};

function isIphone () {
  return !!navigator.userAgent.match(/iPhone/i);
};

function isIpod() {
  return !!navigator.userAgent.match(/iPod/i);
};

function isAppleIos() {
  return (isIpad() || isIphone() || isIpod());
};

function is_touch_device() {
    return !!('ontouchstart' in window) // works on most browsers 
        || !!('onmsgesturechange' in window); // works on ie10
  };

